variable "project_name" {
  default = ""
}

variable "gcp_region" {
  default = ""
}

variable "gcp_zone" {
  default = ""
}

provider "google" {
  credentials = "${file("~/gcp/development-env-creds.json")}"
  project     = "${var.project_name}"
  region      = "${var.gcp_region}"
  zone        = "${var.gcp_zone}"
}

### Network Infrastructure ###

variable "subnets" {
  default = ""
}

variable "stack_id" {
  default = ""
}

variable "auto_create_subnetworks" {
  default = ""
}

variable "routing_mode" {
  default = ""
}

resource "google_compute_network" "network" {
  name                    = "${var.stack_id}"
  auto_create_subnetworks = "${var.auto_create_subnetworks}"
  routing_mode            = "${var.routing_mode}"
}

resource "google_compute_subnetwork" "subnetwork" {
  ip_cidr_range = "${lookup(var.subnets[count.index], "cidr")}"
  name          = "${var.stack_id}-${count.index}"
  network       = "${google_compute_network.network.self_link}"
  region        = "${lookup(var.subnets[count.index], "region")}"

  count = "${length(var.subnets)}"
}

resource "google_compute_firewall" "allow_internal" {
  description   = "Allow all internal communication between VM instances"
  name          = "${var.stack_id}-allow-internal"
  network       = "${google_compute_network.network.self_link}"
  source_ranges = ["10.0.0.0/8", "172.16.0.0/12", "192.168.0.0/16"]
  priority      = "65534"

  allow {
    protocol = "all"
  }
}

resource "google_compute_firewall" "allow_ssh" {
  name     = "${var.stack_id}-allow-ssh"
  network  = "${google_compute_network.network.self_link}"
  priority = "65534"

  allow {
    protocol = "tcp"
    ports    = ["22"]
  }
}

resource "google_compute_firewall" "allow_http" {
  name     = "${var.stack_id}-allow-http"
  network  = "${google_compute_network.network.self_link}"
  priority = "65534"

  allow {
    protocol = "tcp"
    ports    = ["80"]
  }

  target_tags = ["http"]
}

resource "google_compute_firewall" "allow_https" {
  name     = "${var.stack_id}-allow-https"
  network  = "${google_compute_network.network.self_link}"
  priority = "65534"

  allow {
    protocol = "tcp"
    ports    = ["443"]
  }

  target_tags = ["https"]
}

### Test VM

data "google_compute_image" "my_image" {
  family  = "centos-7"
  project = "centos-cloud"
}

variable "machine_type" {
  default = ""
}

variable "startup_script" {
  default = ""
}

variable "name" {
  default = ""
}

variable "subnet" {
  default = ""
}

variable "tags" {
  default = ""
}

variable "zone" {
  default = ""
}

resource "google_compute_instance" "vm" {
  "boot_disk" = {
    initialize_params {
      image = "${data.google_compute_image.my_image.self_link}"
    }
  }

  machine_type = "${var.machine_type}"

  metadata {
    "startup-script" = "${var.startup_script}"
  }

  name = "${var.name}"

  "network_interface" = {
    subnetwork    = "${var.subnet}"
    access_config = {}
  }

  tags = [
    "${var.tags}",
  ]

  zone = "${var.zone}"
}
